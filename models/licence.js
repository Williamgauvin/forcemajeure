var Mongoose = require("mongoose");

var Schema = Mongoose.Schema;
let LicenceSchema = new Schema({
    title: String,
    subTitle: String,
    prise: String,
    contente1: String,
    contente2: String,
    contente3: String,
    contente4: String,
    contente5: String,
    about: String,
}, {
    timestamps: true
});
exports.LicenceModel = Mongoose.model("Licence", LicenceSchema);